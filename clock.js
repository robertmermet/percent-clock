window.addEventListener('load', function load() {
    window.removeEventListener('load', load, false);

    const canvas     = document.createElement('canvas'),
          ctx        = canvas.getContext('2d'),
          dayNames   = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
          monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
          fullDay    = 86400;
    
    let dimensions = { radius: 0, offset: 0, stack: undefined },
        fullMonth  = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        clockData  = {
            day          : undefined,
            dayPercent   : 0,
            month        : undefined,
            monthPercent : 0,
            year         : undefined,
            yearPercent  : 0
    };
        
    document.body.appendChild(canvas);
    window.addEventListener('resize', resize);
    resize();

    (function updateClock() {
        requestAnimationFrame(updateClock);
        getValues();
        drawClocks();
    })();

    function resize() {
        ctx.canvas.width  = window.innerWidth;
        ctx.canvas.height = window.innerHeight;
        ctx.translate(canvas.width / 2, canvas.height / 2);
        if (window.innerWidth > window.innerHeight) {
            dimensions.radius = ctx.canvas.width * .14;
            dimensions.offset = dimensions.radius * 2 + ctx.canvas.width * .04;
            dimensions.stack  = 'horizontal';
        } else {
            dimensions.radius = ctx.canvas.height * .14;
            dimensions.offset = dimensions.radius * 2 + ctx.canvas.height * .04;
            dimensions.stack  = 'vertical';
        }
    }

    function getValues() {
        let date        = new Date(),
            month       = date.getMonth(),
            hours       = date.getHours() * 60 * 60,
            minutes     = date.getMinutes() * 60,
            seconds     = date.getSeconds(),
            currentTime = hours + minutes + seconds,
            fullYear    = 31536000,
            yearTime, dayOfMonth;
        clockData.day        = dayNames[date.getDay()];
        clockData.dayPercent = currentTime / fullDay;
        clockData.month      = monthNames[month];
        monthTime            = (date.getDate() - 1) * fullDay + currentTime;
        // Leap year
        if (((clockData.year % 4 === 0) && (clockData.year % 100 !== 0)) || (clockData.year % 400 === 0)) {
            fullMonth[1] = 29;
            fullYear     = 31622400;
        }
        clockData.monthPercent = monthTime / (fullMonth[month] * fullDay);
        clockData.year         = date.getFullYear();
        yearTime               = monthTime;
        for (let i = 0; i <= (month - 1); i++) {
            yearTime += fullMonth[i] * fullDay;
        }
        clockData.yearPercent = yearTime / fullYear;
    }

    function drawClocks() {
        // Clear canvas
        ctx.fillStyle = '#ccc';
        ctx.fillRect(-canvas.width, -canvas.height, canvas.width * 2, canvas.height * 2);
        // Draw clock faces
        ctx.strokeStyle = '#333';
        ctx.lineWidth   = dimensions.radius * .01;
        ctx.lineCap     = 'round';
        DrawClockFace('month', 0, 0);
        if (dimensions.stack == 'horizontal') {
            DrawClockFace('day', -dimensions.offset, 0);
            DrawClockFace('year', dimensions.offset, 0);
        } else {
            DrawClockFace('day', 0, -dimensions.offset);
            DrawClockFace('year', 0, dimensions.offset);
        }
    }

    function DrawClockFace(type, x, y) {
        ctx.save();
        ctx.translate(x, y);
        ctx.fillStyle = '#fff';
        ctx.beginPath();
        ctx.arc(0, 0, dimensions.radius, 0, 2 * Math.PI);
        ctx.fill();
        // Draw elapsed time
        ctx.fillStyle = '#eee';
        ctx.rotate(-90 * Math.PI / 180);
        ctx.beginPath();
        ctx.moveTo(0, 0);
        ctx.arc(0, 0, dimensions.radius, 0, (clockData[type + 'Percent'] * 2) * Math.PI);
        ctx.fill();
        ctx.fillStyle = '#000';
        ctx.beginPath();
        ctx.arc(0, 0, dimensions.radius * .9, 0, (clockData[type + 'Percent'] * 2) * Math.PI);
        ctx.stroke();
        // Draw indices
        for (let i = 0; i < 20; i++) {
            let angle = i * Math.PI / 10;
            ctx.beginPath();
            ctx.rotate(angle);
            ctx.moveTo(i % 2 == 0 ? dimensions.radius * .82 : dimensions.radius * .86, 0);
            ctx.lineTo(dimensions.radius * .895, 0);
            ctx.stroke();
            ctx.rotate(-angle);
        }
        // Draw dots
        ctx.fillStyle = '#333';
        for (let i = 0; i < 100; i++) {
            let angle = i * Math.PI / 50;
            ctx.beginPath();
            ctx.rotate(angle);
            ctx.arc(dimensions.radius * .95, 0, (i == 0 || i % 5 == 0) ? dimensions.radius * .01 : dimensions.radius * .006, 0, 2 * Math.PI);
            ctx.fill();
            ctx.rotate(-angle);
        }
        // Print text
        ctx.rotate(90 * Math.PI / 180);
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.font = dimensions.radius * .14 + 'px monospace';
        ctx.fillText('Percentage of', 0, -dimensions.radius * .2);
        ctx.fillText(clockData[type] + ' elapsed:', 0, 0);
        ctx.font = dimensions.radius * .2 + 'px monospace';
        ctx.fillText((clockData[type + 'Percent'] * 100).toFixed(2) + '%', 0, dimensions.radius * .24);
        // Draw border
        ctx.beginPath();
        ctx.arc(0, 0, dimensions.radius, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.restore();
    }

}, true);