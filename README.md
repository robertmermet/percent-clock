# Percent Clock

Displays the percentage of time elapsed today, this month and this year.

Uses Ubuntu Mono typeface by Dalton Maag Ltd.

#### Clone Repo

    git clone git@gitlab.com:robertmermet/percent-clock.git

#### View Demo

>**demo** [robertmermet.com/projects/percent-clock](http://robertmermet.com/projects/percent-clock)